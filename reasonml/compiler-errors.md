# ReasonML Compiler Errors

The OCaml/BuckleScript/ReasonML compilers do not provide good error 
messaging when they fail to compile. In the absence of good error messages,
it is important to have a good idea why the compiler fails in order to resolve 
issues. There will be times of frustration, but with practice you should be able 
to develop a good understanding of why it fails.

## Syntax Errors

This is probably the most common error, even when using an IDE.

ReasonML uses JavaScript style syntax. With lots of functional calls and type
signatures, your code will end of with a lot of parentheses. It can be easy to 
misbalance the parentheses and the compiler may give you errors on unrelated line 
numbers. 

To avoid this make small changes when possible and recompile frequently to
reassure your self you have not made a syntax error. If the compiler fails and
you do not understand why, try stepping back to when it compiled and rebuild the
new code piece by piece. This should help reveal where the syntax is missing.

## Type Errors

The compiler is good at type errors.

For example:

```reason
let hello = (x: string) => {
  ...
};

hello(1);
```

## Unclear Type Error

The compiler may have trouble interpreting types in higher order functions.

```reason
type Person = {name: string, age: int};

let people = [{name: "Simon", age: 25}, {name: "Jessica", age: 19}];

List.filter((person) => person.age > 20, people);
```

It may not be clear to the anonymous function in `List.filter` that the type of 
person is `person`. It may be necessary to annotate it.

```reason
List.filter((person: person) => person.age > 20, people);
```

## ReasonReact Errors

Some common ReasonReact errors.

### State Errors

In a ReasonReact class, if your state is a field with one record, you may 
need to annotate the type `: state` in some parts of the ReasonReact class.

```reason
type state = { message : string };
```

In a ReasonReact hook, if the dispatcher is not well defined, it may complain 
about the state type and give you unrelated line numbers for where the error
occurs.

### JSX Errors

You cannot have a raw string in JSX.

```reason
<div>Hello World!</div>
```

You need to return it as a ReasonReact value.

```reason
<div>(ReasonReact.string("Hello World!")</div>
```

Remember that HTML `class` is JSX `className` and HTML `type` is JSX `type_`.

To use an `if` statement in JSX it must have an `else` clause. If you want it to
return nothing, then use `ReasonReact.null`.

```reason
<div>
  (if (state.loggedIn) {
    <div>(ReasonReact.string("You are logged in!"))</div>;
  } else {
    ReasonReact.null;
  })
</div>
```

A list of array of ReasonReact elements must be converted to a single element
with `ReasonReact.array`.

```reason
List.map((msg) => <div>ReasonReact.string(msg)</div>, ["hello", "goodbye"]) |> Array.of_list |> ReasonReact.array;

Array.map((msg) => <div>ReasonReact.string(msg)</div>, [|"hello", "goodbye"|]) |> ReasonReact.array;
```

### A Reason hook called in a Reason class

Reason hooks represent Reason components as functions as opposed to classes.
Reason hooks can use Reason classes, but Reason classes cannot use Reason hooks.
