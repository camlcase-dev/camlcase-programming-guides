# ReasonML Functions

## Single line function

```reason
let sum = (x, y) => x + y;
```

## Multi line function

```reason
let sum = (x, y) => {
  let z = x + y;
  z
};
```


```reason
let sum = x => y => {
  let z = x + y;
  z
};
```


## Type Annotations


```reason
let sum = (x: int, y: int) : int => {
  let z = x + y;
  z
};
```


## Currying

```reason
let plusOne = sum(1);
```
