# JS.Promise callbacks

https://bucklescript.github.io/bucklescript/api/Js_promise.html

Unfortunately ReasonML does not offer an `await` like solution from ES6 for 
promises. We are required to use callback style when handling promises. Any
non-blocking request to a server will likely use the type `Js.Promise.t`.
[bs-fetch](https://github.com/reasonml-community/bs-fetch). Types still have to match.

```
Js.Promise._then()
Js.Promise.all()
Js.Promise.catch()
resolve()
```

