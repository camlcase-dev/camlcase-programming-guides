# ReasonML Backend Router

```reason
/** useful when multiple servers are involved (testing, staging, production, etc.) */
module type Config = {let baseUrl: string;};

module API = (Config: Config) => {
  type backendRoute =
    | Hello
    | Login
    | User;

  let toRoute = (route: backendRoute): string => {
    Config.baseRoute ++ switch (route) {
      | Hello => "/hello"
      | Login => "/login"
      | User  => "/user"
    };
  };
  
  let getHello = (): Js.Promise.t(Js.Json.t) => {
    let url = toRoute(Hello);
    let headers =
      Fetch.HeadersInit.makeWithArray([|
        ("Accept", "application/json"),
        ("Content-Type", "application/json"),
      |]);
    Js.Promise.(
      Fetch.fetchWithInit(
        url,
        Fetch.RequestInit.make(
          ~method_=Get,
          ~headers,
          ~credentials=Include,
          (),
        ),
      )
      |> then_(Fetch.Response.json)
      |> then_(json =>
           resolve(json)
         )
  };
};

module Fetch =
  API.MakeAPIFetch({
    let baseUrl = "127.0.0.1";
  });

Fetch.getHello() |> Js.Promise.then_(result => { Js.Console.log(result); Js.Promise.resolve();})
```
