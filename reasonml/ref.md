# ReasonML Refs

Refs are containers of mutable values. They have a particular syntax in ReasonML 
you need to know in order to use them. Generally they should be avoided when there
is a clean functional solution, but they can be useful for certain problems.

```reason
/** declare a ref */
let i = ref(0);

/** get the value of a ref */
Js.Console.log(i^);
let x = i^;

/** set the value of a ref */
i := i^ + 1;
Js.Console.log(i^);

```

## Loop

Generally you should use `map`, `filter`, `reduce`,
but for algorithmic performance loops may be a better solution if they end 
iteration early under certain conditions.

Integer ref:

```reason
let i = ref(0);

while (^i < 10) {
  Js.Console.log(i^);
  i := ^i + 1;
};
```

Boolean ref:

```reason
Random.self_init();

let break = ref(false);

while (! break^) {
  let x = Random.int(10);
  if (x === 3) {
    Js.Console.log("The random value was equal to 3. The while loop will end.");
    break := true
  } else {
    Js.Console.log("The random value was equal to " ++ string_of_int(x) ++ ". The while loop will run again.");
    print_endline("hello")
  }
};
```
