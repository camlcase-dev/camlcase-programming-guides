# ReasonML Programming Guide

## Index

- [Compiler Errors](./compiler-errors.md)
- [Functions](./functions)
- [Types](./types)
- [Hashmap and Set](./hashmap-and-set.md)
- [Option and Result](./option-and-result.md)
- [List and Array](./list-and-array.md)
- [JavaScript FFI](./javascript-ffi.md)
- [Js.Promise Callbacks](./js-promise-callbacks.md)
- [Map, Filter, Reduce](./map-filter-reduce.md)
- [Modules](./modules)
- [ReasonReact Classes](./react-classes.md)
- [ReasonReact Hooks](./react-hooks.md)
- [Backend Router](./backend-router.md)
- [Serialization](./serialization.md)

## Introduction

camlCase's opinionated guide to ReasonML, ReasonReact and ReasonReactNative.

https://reasonml.github.io/reason-react/
https://reasonml-community.github.io/reason-react-native/en/docs/
[Reason Package Index](https://redex.github.io/)

ReasonML is a syntax and toolchain powered by OCaml that compiles to JavaScript
with tools focused on a browser environment and access to already existing 
JavaScript environments. It uses JavaScript like syntax and uses NPM/Yarn for
package management.

OCaml is a functional programming language that was created in 1996.

BuckleScript is a compiler based on OCaml 4.02.3 that compiles OCaml source
code into JavaScript.

ReasonML is a JavaScript like syntax for the BuckleScript compiler. It also
supports JSX (JSX is not supported by BuckleScript). Projects may have both
OCaml syntax files, ending with `.ml` or `.mli`, and ReasonML syntax files, 
ending with `.re` or `.rei`.

ReasonML does not require you to provide an interface file, `.rei`. However, they
are useful if you want to restrict what types and values are exported.

## Motivation for Using ReasonML

- [ReasonML - What and Why?](https://reasonml.github.io/docs/en/what-and-why)

## Built-in libraries

Libraries included with BuckleScript and ReasonML.

- [OCaml 4.02.3 Standard Library](https://caml.inria.fr/pub/docs/manual-ocaml-4.02/stdlib.html)
- [OCaml 4.02.3 Pervasives](https://caml.inria.fr/pub/docs/manual-ocaml-4.02/libref/Pervasives.html) (these are accessed without any module/namespace prefixing the function name)
- [Js](https://bucklescript.github.io/bucklescript/api/Js.html)
- [Belt](https://bucklescript.github.io/bucklescript/api/Belt.html)
- [Dom](https://bucklescript.github.io/bucklescript/api/Dom.html)

## Front End Libraries

- [ReasonML](https://reasonml.github.io/)
- [ReasonReact](https://reasonml.github.io/reason-react/en/)
- [ReasonReactNative](https://reasonml-community.github.io/reason-react-native/)

## Building and Development Tools

### IDE

Visual Studio Code and vim are well supported for ReasonML. The emacs plugin is
currently unsupported.

- [Editor Plugins](https://reasonml.github.io/docs/en/editor-plugins)

### refmt

`refmt` is a command line tool included with the bs-platform that reformats 
ReasonML code. This is what we use to style our code indentation and parentheses.
However, it ocassionally breaks code and has trouble with certain structures so 
be careful. Most of the editor plugins use `refmt`. It can also help you 
translate between `.ml` and `.re` files. 

#### Common Commands

- `refmt --help`, how to used `refmt`.

- `refmt file-path`, read a file and output reformated version to stdout.

- `refmt file-path --in-place`, read a file and overwrite it with the reformatted version.

### package.json

Use Yarn over npm. These are styled quite simlarly to a JavaScript project,
but should have a lot less dependencies because of the functionality built
into `bs-platform`.

### bsconfig.json

This is a build file needed by `bsb`. Any ReasonML packages you want to depend 
on must be declared here as well as in `package.json`.

- [Important Field in bsconfig](https://bucklescript.github.io/docs/en/build-configuration.html)
- [BuckleScript build configuration](https://bucklescript.github.io/bucklescript/docson/#build-schema.json)

## General Code Style Tips

## Camel Case

We use caml case for function and value names, which follows the preference 
found in BuckleScript and ReasonML specific code. In the OCaml standard library, 
they use snake case.

## Type Annotations

In ReasonML it is easy to ignore return types because the compiler is often 
sophisticated enough to guess what the type should be, but it makes it harder to
read.

Without annotations, it is hard to read, the types are not clear:
```reasonml
let mapOk = (f, xs) => {...}
```

Verbose but easier to understand what the types are:
```reasonml
let mapOk = (f: ('a => 'b), xs: list(Belt.Result.t('a,'c))) : list(Belt.Result.t('b,'c)) => {...}
```

## Adding new types, functions and values

Whenever you want to add a new type, function or value that can potentially be
used in multiple places, go through the following check list first.

- If it exist in one of the shared libraries (OCaml stdlib, Belt, Js, Dom, 
    ReasonReact), then use it from one of those sources, do not needlessly 
    reduplicate code.

- If it is needed for one or more files, do not define it in a top level file 
    or component file. Find or create a file to share it between files that need
    it. That way we can avoid reduplication.

## Avoid Partial Functions from the stdlib

Unfortunately the stdlib has a number of partial functions that will produce
run time errors. Really they should return an option type.

Do not use the following functions:

- List.hd
- List.tl
- List.nth
- int_of_string
- float_of_string

Instead use:

- Belt.List.head
- Belt.List.tail
- Belt.List.get
- Belt.Int.fromString
- Belt.Float.fromString
