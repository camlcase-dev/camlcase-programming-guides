# Hashmap and Set

For a list of unique values, we use `Belt.Set`.

For key-value pairs, we use `Belt.Map`.

Both of these require a `Belt.Id` module declaration in order to tell it how to
compare the eunique values.

## Belt.Set

- [Belt.Set](https://bucklescript.github.io/bucklescript/api/Belt.Set.html)

Define your own set.

```reason
module ComparableIntPair =
  Belt.Id.MakeComparable({
    type t = (int, int);
    let cmp = ((a0, a1), (b0, b1)) =>
      switch (Pervasives.compare(a0, b0)) {
      | 0 => Pervasives.compare(a1, b1)
      | c => c
      };
  });

let intPairSet = Belt.Set.fromArray([|(1,1),(1,2),(-1,0),(1,1)|], ~id=(module ComparableIntPair),)

Js.Console.log(Belt.Set.size(intPairSet));
Js.Console.log(Belt.Set.isEmpty(intPairSet));
Js.Console.log(Belt.Set.get(intPairSet, (1,1)));
```

`type t` is the type that will be stored in the set.

`cmp` takes two `t`s and must return `-1`, `0`, `1`.

You can also use two built in set declaractions for ints and strings, `Belt.SetInt` and `Belt.SetString`.

## Belt.Map

- [Belt.Map](https://bucklescript.github.io/bucklescript/api/Belt.Map.html)

Define your own map.

```reason
type key = { key : string };

module ComparableKey =
  Belt.Id.MakeComparable({
    type t = key;
    let cmp = (a, b) => Pervasives.compare(a.key, b.key);
  });

let keyValueMap = Belt.Map.fromArray([|({key:"a"},1),({key:"b"},2),({key:"c"},0),({key:"d"},1)|], ~id=(module ComparableKey),);

Js.Console.log(Belt.Map.size(keyValueMap));
Js.Console.log(Belt.Map.get(keyValueMap, {key:"a"}));
Js.Console.log(Belt.Map.get(keyValueMap, {key:"e"}));
Js.Console.log(Belt.Map.has(keyValueMap, {key:"a"}));
```

You can also use two built in map declaractions for ints and strings, `Belt.MapInt` and `Belt.MapString`.
